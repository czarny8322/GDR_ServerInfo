﻿using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;
using Travelport.Smartpoint.Common;
using Travelport.Smartpoint.Controls;
using Travelport.Smartpoint.Helpers.UI;
using Travelport.Smartpoint.Controls.Pnr.Views;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows;
using serverInfo.Helpers;
using serverInfo.Views;
using System.Text.RegularExpressions;
using System;
using System.Net;

namespace serverInfo.ViewModels
{
    public class ViewModel : ViewModelBase
    {
        //public string lastIpDigits { get; set; }

        public ViewModel(ISmartTerminalWindow terminalWindow)
        {
            Regex regex = new Regex(@"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}");
            
            var response = UIHelper.Instance.CurrentTEControl.Connection.SendEntry("+H");
            foreach (var i in response)
            {                
                Match match = regex.Match(i);
                if (match.Success)
                {
                    InfoMessage = match.Value;
                    LastIpDigits = getLastSegment(InfoMessage);

                    break;
                }
                else
                {
                    InfoMessage = "No Info";
                    LastIpDigits = "SRV";
                }
            }
        }

        private string getLastSegment(string infoMessage)
        {
            return IPAddress.Parse(infoMessage).GetAddressBytes()[3].ToString();           
        }

        public void CreateToolMenu()
        {           
            var toolbarButton = CreateSmartPnrCustomToolbarButton();
            if (!UIHelper.Instance.CurrentSmartTerminalWindow.PnrCustomToolbar.Contains(toolbarButton))
                UIHelper.Instance.CurrentSmartTerminalWindow.PnrCustomToolbar.Add(toolbarButton);
        }
        public SmartPnrCustomToolbarButton CreateSmartPnrCustomToolbarButton()
        {
            var bbushConv = new BrushConverter();

            

            var icon = new System.Windows.Controls.Image()
            {
                Source = BitmapToImageSource(Properties.Resources.Error.ToBitmap()),
                RenderSize = new System.Windows.Size(40, 40)
            };

           

            var customButton = new PnrCustomToolbarFindInPnr() {

                //ButtonContentDefault = new TextBlock { Text = lastIpDigits, Foreground = (System.Windows.Media.Brush)bbushConv.ConvertFrom("#FFFFFF"), FontWeight = FontWeights.ExtraBold, FontSize = 14, Background = new SolidColorBrush(Colors.Transparent) },
                //ButtonContentSelected = new TextBlock { Text = lastIpDigits, Foreground = (System.Windows.Media.Brush)bbushConv.ConvertFrom("#FF3781C3"), FontWeight = FontWeights.ExtraBold, FontSize = 14, Background = new SolidColorBrush(Colors.Transparent) },
                //ButtonContentHover = new TextBlock { Text = lastIpDigits, Foreground = (System.Windows.Media.Brush)bbushConv.ConvertFrom("#FF4E4E4E"), FontWeight = FontWeights.ExtraBold, FontSize = 14, Background = new SolidColorBrush(Colors.Transparent) },
                //ButtonContentClick = new TextBlock { Text = lastIpDigits, Foreground = (System.Windows.Media.Brush)bbushConv.ConvertFrom("#FF4E4E4E"), FontWeight = FontWeights.ExtraBold, FontSize = 14, Background = new SolidColorBrush(Colors.Transparent) },
                
                ButtonContentDefault = DrawIcon("orange"),
                ButtonContentSelected = DrawIcon("blue"),
                ButtonContentHover = DrawIcon("blue"),
                ButtonContentClick = DrawIcon("blue"),

                ToolTip = "GDR Server Index",
                Background = new SolidColorBrush(Colors.Blue),
                View = new View(),
                DataContext = this
            };
            
            return customButton;
        }

        public BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();
                return bitmapimage;
            }
        }

        private string _infoMessage;
        public string InfoMessage
        {
            get
            {
                return _infoMessage;
            }
            set
            {
                _infoMessage = value;
                OnPropertyChanged("InfoMessage");
            }
        }

        private string _lastIpDigits;
        public string LastIpDigits
        {
            get
            {
                return _lastIpDigits;
            }
            set
            {
                _lastIpDigits = value;
                OnPropertyChanged("LastIpDigits");
            }
        }


        public Grid DrawIcon(string color)
        {
            var g = new Grid();
            var icon = "";

            if (color == "orange")
            {
                icon = "iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAYAAAA+s9J6AAAABmJLR0QA/wD/AP+gvaeTAAAACXBI" +
                            "WXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4gcNDjEAG9E+XAAAAzpJREFUeNrt3O1KG0EYhuF3drfn" +
                            "KAg9uIjQc5Ts9IeiFhJZGzPvzux1/RJpsd3kzjP5wAgAAAAAAAAAAAAAAAAAYHTllr9cn6O6hBw+" +
                            "osfbOirCg9wgi/ggN8YiQMiNsQgQckMs4oPcECeXB+7vq1ErVhByF7EIEHJDLAKE3BA9J4TsIK0g" +
                            "5K6hJYRkIoQ9HEcdRSHvSGoJwXEURAiIEEQIiBBECIgQjmfZ/b9wPfgt5GMU91Xyp2hJjat+imy+" +
                            "ss+/NlzHh9t+7yMHf5w7Rc2MMSfCc0T5LRx2MoZv98V6ivrPGAz5nHAVIDuP8dz+KVDbCKsA6SDE" +
                            "OmqEFpDeFnGoCFcB0mGI60gRepmdHtVRIrSC9GqYJbSCsIPjKPRoFiFYQkCEMHwdIgRLCCIELpmj" +
                            "yXuFIgRLCCIERAgiBEQIIgRECCIE3rT6fbYiBEsIIgRECCIERAgiBEQIIgRECCIERAgiBEQIIgRE" +
                            "CCIERAgiBEQIIgRECCIERAiNVRGCJQRECCIERAgiBEQIIgRECCIERAgiBEQIIgRECCIERAgiBEQI" +
                            "IgRECCIERAgiBEQIIgRECCIERAgiBEQIIgRECCIERAgiBEQIIgRECCIERAgiBEQIIgRECCIERAgi" +
                            "BEQIIgQRAiIEEQIiBBECIgQRAiIEEQIiBBECIgQRAiIEEQIiBBECIgQRAiIEEQIiBBECIgQRAiIE" +
                            "EQIiBBECIgQRAiIEEQIiBBECIgQRAjuNsLjIdKyMEOEUUU9R3Zr0pp6itjgrtjmOWkOsYHKE1hAr" +
                            "mBxhRMQsRDoKcG7389q+OlqESAcBNn76NDX/aRaRvS9g4zfulpT/7RxRn6JGjYj143sXHxo2HAvK" +
                            "g5d+DhvOnw0P6OdPX68Xvj+9ntKy3jVf0q7etDGylw03xJNlPayXb9zXvvo60bL7i+wzPQzOXRxE" +
                            "CCIERAgiBEQIIgRECCIEGnv/zGV99tEvaBrf42t/lhAcR8FxNBxJIecoaglhb0toDaHtCl6MUIjQ" +
                            "LkDHUUgO8GqE1/4w0OA5oaMptFnAzREKEe4X4OYIhQg/H9+3IxQj/HyA/xWhIOG26AAAAAAAAAAA" +
                            "AAAAAACA4/kLl/SLhK70bPIAAAAASUVORK5CYII=";
            }
            if (color == "blue")
            {
                icon = "iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAYAAAA+s9J6AAAABmJLR0QA/wD/AP+gvaeTAAAACXBI" +
                            "WXMAAA7EAAAOxAGVKw4bAAAAB3RJTUUH4gcNDjAJexa3uQAAApFJREFUeNrt3LFxwkAQhlGtRrXQ" +
                            "ESnFkdIRzeAIO8D2YEvcf+jeywkQ982uBsE0AQAAAAAAAAAAAAAAAAB7V2tefDxfby4ho7ucDtU0" +
                            "QuHBtkGW+CAbYwkQsjGWACEbYokPsiHOLg+83m9DrUxByE7EEiBkQywBQjZE94QQVqYgZKehSQhh" +
                            "IoQe1lGrKORWUpMQrKMgQkCEIEJAhCBCQIQgQqCxZQ9vYu3/PjK29BNji3AY3f0spmKcBQjZczkL" +
                            "ELLncxYgZM/pLEDInldfUUBYkwhNQTAJQYSACEGEgAhBhIAIQYSACEGEgAhBhIAIQYQgQkCEIELg" +
                            "Qasfo4sQTEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQ" +
                            "ISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAhIEIQISBCECEgQhAh" +
                            "IEIQISBCECEgQhAhIEIQISBCECGI0CUAEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAER" +
                            "gggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGCCAERgggBEYIIARGC" +
                            "CIFOIjyerzeXmnfT6tyahDDKOmoaYgp2MAmFiAA7WEeFiAA7uCcUIgL8suzhDV9Oh3KEhPOuFh8E" +
                            "ZPmKAkQIIgRECCIERAgiBEQIIgRECGP5fObSo1/Q1v2ZZ5MQrKNgHZ2spJBZRU1C6G0SmobQdgp+" +
                            "G6EQoV2A1lEIB/hjhP6zBYL3hFZTaDMBn45QiPC6AJ+OUIiwfXx/jlCMsH2A/4pQkLAuOgAAAAAA" +
                            "AAAAAAAAAABgPB9Rq5F3/3HS6gAAAABJRU5ErkJggg==";
            }

            

            var bi = new System.Windows.Media.Imaging.BitmapImage();
            bi.BeginInit();
            bi.StreamSource = new System.IO.MemoryStream(Convert.FromBase64String(icon));
            bi.EndInit();
            var img = new System.Windows.Controls.Image { Width = 32, Height = 32, Source = bi };
            g.Children.Add(img);
            var i = 0;

            var textBox = new TextBlock { FontSize = 15, FontWeight = FontWeights.Bold, Foreground = System.Windows.Media.Brushes.White, Text = LastIpDigits, VerticalAlignment = VerticalAlignment.Center, HorizontalAlignment = HorizontalAlignment.Center };
            g.Children.Add(textBox);


            //System.Threading.Tasks.Task.Factory.StartNew(() =>
            //{
            //    while (i < 100)
            //    {
            //        UIHelper.Instance.CurrentSmartTerminalWindow.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Background,
            //            new Action(delegate
            //            {
            //                textBox.Text = i.ToString();
            //                i++;
            //            }));
            //        System.Threading.Thread.Sleep(1000);
            //    }
            //});
            //var testButton = new Travelport.Smartpoint.Controls.SmartCommandToolbarButton();
            //testButton.ButtonContentDefault = g;
            //UIHelper.Instance.CurrentSmartTerminalWindow.CommandToolbar.Add(testButton);
            return g;
        }

    }

}
