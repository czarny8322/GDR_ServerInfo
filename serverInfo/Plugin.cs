﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Plugin.cs" company="Name of the company">
//  Copyright (c). All rights reserved.
// </copyright>
// <summary>
//   Plugin for Travelport smartpoint
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace serverinfo
{
    using Hardcodet.Wpf.TaskbarNotification;
    using serverInfo.Properties;
    using serverInfo.ViewModels;
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using System.Windows.Media;
    using Travelport.Smartpoint.Common;
    using Travelport.Smartpoint.Helpers.Core;
    using Travelport.Smartpoint.Helpers.UI;

    /// <summary>
    /// Plugin for extending Smartpoint application
    /// </summary>
    [SmartPointPlugin(Developer = "Adam Stawarz",
                      Company = "Travelport",
                      Description = "Small plugin for show server IP",
                      Version = "1.0",
                      Build = "Build version of Smartpoint application")]
    public class Plugin : HostPlugin
    {


        /// <summary>
        /// Executes the load actions for the plugin.
        /// </summary>
        public override void Execute()
        {
            // Attach a handler to execute when the Smartpoint application is ready and all windows are loaded.
            CoreHelper.Instance.OnSmartpointReady += this.OnSmartpointReady;

        }

        #region Handlers

        private void OnCreateSmartpointWindowHandler(object sender, EventArgs e)
        {
            var terminalWindow = sender as ISmartTerminalWindow;
            AddChatPlugin(terminalWindow);
        }

        /// <summary>
        /// Handles the Smartpoint Ready event of the Instance control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="Travelport.Smartpoint.Helpers.Core.CoreHelperEventArgs"/> instance containing the event data.</param>
        private void OnSmartpointReady(object sender, CoreHelperEventArgs e)
        {
            var terminalWindow = UIHelper.Instance.CurrentSmartTerminalWindow;
            AddChatPlugin(terminalWindow);
            UIHelper.Instance.OnCreateSmartpointWindow += OnCreateSmartpointWindowHandler;
            
        }

        #endregion

        private void AddChatPlugin(ISmartTerminalWindow terminalWindow)
        {
            if (terminalWindow == null) return;
            var vm = new ViewModel(terminalWindow);
            vm.CreateToolMenu();
        }
    }
}
